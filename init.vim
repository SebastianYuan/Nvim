if has('win32') || has('win64')
    source $HOME\AppData\Local\nvim\configs\setting.vim
    source $HOME\AppData\Local\nvim\configs\keybindings.vim
    source $HOME\AppData\Local\nvim\configs\rainbow-parenthese.vim
else
    source $HOME/.config/nvim/configs/plugins.vim
    source $HOME/.config/nvim/configs/setting.vim
    source $HOME/.config/nvim/configs/keybindings.vim
    source $HOME/.config/nvim/configs/misc.vim
    source $HOME/.config/nvim/configs/rainbow-parenthese.vim
    source $HOME/.config/nvim/configs/coc_setting.vim
endif


