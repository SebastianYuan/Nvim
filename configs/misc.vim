" Setting for indentLine Plugin
" Enable indent line guide
let g:indentLine_concealcursor='inc'
let g:indentLine_fileTypeExclude=['tex', 'json']
let g:indentLine_char='▏'
let g:indentLine_setColors=1
let g:indentLine_setConceal=0

" Airline Theme
let g:airline_theme='tomorrow'

" Airline setup "
let g:airline#extension#branch#enable=1
let g:airline_powerline_fonts=1
let g:airline#extensions#branch#vcs_priority=["git", "mercurial"]
let g:airline#extensions#branch#displayed_head_limit=10
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#left_sep = ""
let g:airline#extensions#tabline#left_alt_sep = "|"

" Python Format on Save with Black
autocmd BufWritePre *.py execute ':Black'

" Auto code Folding for Javascript
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END
